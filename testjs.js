if (ResponseData != null && typeof (ResponseData.Status.authenticationData.accessToken) != 'undefined' && ResponseData.Status.authenticationData.accessToken != null && typeof (ResponseData.Status.authenticationData.accountName) != 'undefined' && ResponseData.Status.authenticationData.accountName != null) {
                var Params = {};
                Params.accessToken = ResponseData.Status.authenticationData.accessToken;
                Params.accountName = ResponseData.Status.authenticationData.accountName;
                Params.refreshToken = ResponseData.Status.authenticationData.refreshToken;
                Params.authenticationData = ResponseData.Status.authenticationData;
                Params.repoPath = ResponseData.Status.authenticationData.gitHubDefaultRepoId.value;
                Params.userIntegrationID = ResponseData.Status._id;
                Params.ResponseData = ResponseData.Status;
                Params.filePath = params.emitData.file.path
                if (typeof (params.data.DataArray[3]) != 'undefined' && params.data.DataArray[3].toLowerCase() == 'by') {
                    if (typeof (params.data.DataArray[4]) != 'undefined' && params.data.DataArray[4].toLowerCase() == 'url' && typeof (params.data.DataArray[5]) != 'undefined' && params.data.DataArray[5] != '') {
                        Params.url = params.data.DataArray[5];
                        CurrentObj.getGitHubFileContentsByURL(Params, CurrentObj.GetGitHubFileContentCallBack.bind(null, CurrentObj, IntegrationApisData, io, conf, Params, callback));
                    } else {
                        Params.filePath = params.data.DataArray[3];
                        Params.versionType = params.data.DataArray[4];
                        Params.version = params.data.DataArray[5];
                    }
                } else {
                    callback({ status: "Please provide a valid syntax. Type <b>/visualstudio help </b> to to get a list of activities for VisualStudio." });
                }
            } else {
                var StatusMessage = integrationAuth.IntegratoinErrorMessage(conf, params);
                callback({ status: StatusMessage });
            }